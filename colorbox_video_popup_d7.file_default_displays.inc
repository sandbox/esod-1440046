<?php
/**
 * @file
 * colorbox_video_popup_d7.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function colorbox_video_popup_d7_file_default_displays() {
  $export = array();

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_vimeo_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'video_thumbnail',
  );
  $export['video__default__media_vimeo_image'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_vimeo_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autoplay' => 1,
  );
  $export['video__default__media_vimeo_video'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'video_thumbnail',
  );
  $export['video__default__media_youtube_image'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autoplay' => 1,
  );
  $export['video__default__media_youtube_video'] = $file_display;

  return $export;
}
