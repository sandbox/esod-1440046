<?php
/**
 * @file
 * colorbox_video_popup_d7.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function colorbox_video_popup_d7_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = '';
  $context->tag = 'front_page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-video-block_1' => array(
          'module' => 'views',
          'delta' => 'video-block_1',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('front_page');
  $export['front_page'] = $context;

  return $export;
}
